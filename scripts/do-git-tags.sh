#!/bin/sh

# Usage: ./scripts/do-git-tags.sh 1.1.0 [push]

BRANCH="$(git rev-parse --abbrev-ref HEAD)"
if [[ "$BRANCH" != "main" ]]; then
  echo 'Tags should be set on the "main" branch.'
  exit 1
fi

if [[ -z "$1" ]]; then
  echo "No argument supplied"
  exit 1
fi

if [[ "$2" == "push" ]]; then
  DRY_RUN=0
  echo "\nDry-run mode is OFF. Git commands will be executed."
else
  DRY_RUN=1
  echo "\nDry-run mode is ON. No git commands will be executed."
fi

# Compute tags.
TAG="$1"
IFS=. read major minor micro <<<"${TAG}"
MINOR_TAG="${major}.${minor}.x-latest"
MAJOR_TAG="${major}.x-latest"

if [[ -z "$major" || -z "$minor" || -z "$micro" ]]; then
  echo "Tag format is not valid."
  exit 1
fi

echo "\nSetting tag: $TAG"
if [[ "$DRY_RUN" == "1" ]]; then
  echo "- git tag $TAG"
  echo "- git push origin $TAG"
else
  git tag $TAG
  git push origin $TAG
fi

echo "\nSetting latest minor tag: $MINOR_TAG"
if [[ "$DRY_RUN" == "1" ]]; then
  echo "- git tag -d $MINOR_TAG || TRUE"
  echo "- git push origin --delete $MINOR_TAG || TRUE"
  echo "- git tag $MINOR_TAG"
  echo "- git push origin $MINOR_TAG"
else
  git tag -d $MINOR_TAG || TRUE
  git push origin --delete $MINOR_TAG || TRUE
  git tag $MINOR_TAG
  git push origin $MINOR_TAG
fi

echo "\nSetting latest major tag: $MAJOR_TAG"
if [[ "$DRY_RUN" == "1" ]]; then
  echo "- git tag -d $MAJOR_TAG || TRUE"
  echo "- git push origin --delete $MAJOR_TAG || TRUE"
  echo "- git tag $MAJOR_TAG"
  echo "- git push origin $MAJOR_TAG"
else
  git tag -d $MAJOR_TAG || TRUE
  git push origin --delete $MAJOR_TAG || TRUE
  git tag $MAJOR_TAG
  git push origin $MAJOR_TAG
fi
